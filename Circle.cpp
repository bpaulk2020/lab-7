#include <cmath>
#include "Circle.h"
#include "iostream"

using namespace std;


Circle::Circle(int r): r(r) {}

void Circle::area() {
    double area = M_PI * pow(r,2);
    printf("Area of circle is %.2f", area);
}

void Circle::perimeter() {
    double perimeter = 2 * M_PI * r;
    printf("Circumference of circle is %.2f", perimeter);
}

void Circle::volume() {
    printf("Volume is not defined for circle.\n");
}
