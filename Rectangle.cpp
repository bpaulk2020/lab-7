#include <cmath>
#include "Rectangle.h"
#include "iostream"

using namespace std;


Rectangle::Rectangle(int length , int width): length(length), width(width) {}

void Rectangle::area() {
    double area = length * width;
    printf("Area of rectangle is %.2f", area);
}

void Rectangle::perimeter() {
    double perimeter = 2 * length + 2 * width;
    printf("Perimeter of rectangle is %.2f", perimeter);
}

void Rectangle::volume() {
    printf("Volume is not defined for circle.\n");
}
