#include <cmath>
#include "Sphere.h"
#include "iostream"

using namespace std;


Sphere::Sphere(int r): r(r) {}

void Sphere::area() {
    double area = 4 * M_PI * pow(r,2);
    printf("Surface Area of sphere is %.2f", area);
}

void Sphere::perimeter() {
    double perimeter = 2 * M_PI * r;
    printf("Circumference of sphere is %.2f", perimeter);
}

void Sphere::volume() {
    double volume = 4/3 * M_PI * pow(r,3);
    printf("Volume of sphere is: ", volume);
}