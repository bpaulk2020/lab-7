#include <cmath>
#include "Cube.h"
#include "iostream"

using namespace std;


Cube::Cube(int length): length(length) {}

void Cube::area() {
    double area = pow(length, 2) * 6;
    printf("Surface Area of Cube is %.2f", area);
}

void Cube::perimeter() {
    double perimeter = 12 * length;
    printf("Perimeter of Cube is %.2f", perimeter);
}

void Cube::volume() {
    double volume = pow(length, 3);
    printf("Volume of Cube is %.2f", volume);
}
