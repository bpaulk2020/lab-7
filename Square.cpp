#include <cmath>
#include "Square.h"
#include "iostream"

using namespace std;


Square::Square(int length): length(length) {}

void Square::area() {
    double area = pow(length, 2);
    printf("Area of square is %.2f", area);
}

void Square::perimeter() {
    double perimeter = 4 * length;
    printf("Perimeter of square is %.2f", perimeter);
}

void Square::volume() {
    printf("Volume is not defined for square.\n");
}
